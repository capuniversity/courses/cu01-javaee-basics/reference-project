package com.acme.trainings.domain.repositories;

import com.acme.trainings.domain.model.Book;
import com.acme.trainings.domain.repositories.BookRepository;
import com.acme.trainings.rs.controllers.BooksRestController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class BooksRepositoryMockitoTest {

    BooksRestController controller;
    private BookRepository cut;
    private List<Book> mockedBookList = new ArrayList<>();

    /**
     * Now we forget everything about JAVA EE.
     * We simply test our classes and functions as POJOs, mocking anything outside our test scope.
     *
     */
    @Before
    public void initializeDependencies() {
        //this will never happen in real life, no managed beans are instantiated directly
        cut = new BookRepository();
        //we do not have any DB, so we mock whole entity manager
        cut.em = mock(EntityManager.class);
        Book mockBook = new Book();
        mockBook.setId(1l);
        mockBook.setTitle("The benefits of testing");
        this.mockedBookList.add(mockBook);
        TypedQuery queryByMock = Mockito.mock(TypedQuery.class);
        //mockito magic, when some code invokes method 'createQuery' on our mocked instance of em,
        // then return our mocked query object
        when(cut.em.createQuery(anyString(), any())).thenReturn(queryByMock);
        //when our mocked query is called, just return static list
        when(queryByMock.getResultList()).thenReturn(mockedBookList);

    }

    @Test
    public void getAllBooks() {
        List<Book> result = cut.getAllBooks();
        assertEquals(1, result.size());
        //here we check, that our fake em.createQuery was called exactly once
        verify(cut.em, times(1)).createQuery(anyString(), any());
        //technically we have tested our repo method, but with fake DB, the value of this test is pretty low
    }
}