package com.acme.trainings;

import com.acme.trainings.domain.model.Book;
import com.acme.trainings.domain.repositories.BookRepository;
import com.acme.trainings.rs.JAXRSConfiguration;
import com.acme.trainings.rs.controllers.BooksRestController;
import org.hamcrest.core.IsNull;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.net.URL;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;

/**
 * This test is executed by arquillian. It will start the defined container before executing any of hte test methods.
 * See src/test/resources/arquillian.xml and pom.xml
 */
@RunWith(Arquillian.class)
//Run as Client = run outside of the JBoss server = remote testing,
// use this when you want to test REST layer, interacting with the server as remote client
@RunAsClient
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookRestControllerArquillianTest {

    /**
     * We define the archive that will be deployed during the tests.
     * The archive building is handled by Shrinkwrap library
     * @return
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive warToDeploy = ShrinkWrap.create(WebArchive.class)
                //we can decide which classes we add
                .addPackages(true, "com.acme.trainings.domain")
                .addPackages(true, "com.acme.trainings.rs")
                //we can add static file or config files as needed
                .addAsResource("META-INF/persistence.xml")
                .addAsResource("META-INF/load.sql")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println(warToDeploy.toString(true));
        return warToDeploy;

    }

    //This is a URL of our wildfly server when arquillian starts it up
    @ArquillianResource
    private URL url;


    @Test
    public void test1_getAllBooks() {
        given().when().get(url + "books")
                .prettyPeek()//print response to console
                .then()
                .statusCode(200)//assert status code is 200
                .header("Content-Type", "application/json")//assert response is json
                .body("size()", is(3))//assert the response is an array of 3 items
                .body("[0].id", is(9000))//assert that id of the first book is 9000
                //remember we inserted test data using load.sql script
                .body("[0].tags", is(IsNull.nullValue()));//assert that tags are not returned

    }

    @Test
    public void test2_getBookDetails() {
        given().when().get(url + "books/9001")
                .prettyPeek()//print response to console
                .then()
                .statusCode(200)//assert status code is 200
                .header("Content-Type", "application/json")//assert response is json
                .body("id", is(9001))//assert that id of the first book is 9000
                .body("author.name", is("Sun Tzu"))//assert that author name of book is Sun Tzu
                //remember we inserted test data using load.sql script
                .body("bookTags.size()", is(3));//assert that the book has 3 tags

    }
}
