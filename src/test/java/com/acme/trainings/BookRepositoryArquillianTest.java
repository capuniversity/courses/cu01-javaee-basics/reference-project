package com.acme.trainings;

import com.acme.trainings.domain.model.Author;
import com.acme.trainings.domain.model.Book;
import com.acme.trainings.domain.repositories.AuthorRepository;
import com.acme.trainings.domain.repositories.BookRepository;
import com.acme.trainings.rs.JAXRSConfiguration;
import com.acme.trainings.rs.controllers.BooksRestController;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import javax.inject.Inject;
import java.util.List;

/**
 * This test is executed by arquillian. It will start the defined container before executing any of hte test methods.
 * See src/test/resources/arquillian.xml and pom.xml
 */
@RunWith(Arquillian.class)
//Not ideal, but allows us to run test in deterministic order
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookRepositoryArquillianTest {

    /**
     * We define the archive that will be deployed during the tests.
     * The archive building is handled by Shrinkwrap library
     * @return
     */
    @Deployment
    public static WebArchive createDeployment() {
        WebArchive warToDeploy = ShrinkWrap.create(WebArchive.class)
                //we can decide which classes we add
                .addPackages(true, "com.acme.trainings.domain")
                .addPackages(true, "com.acme.trainings.rs")
                //we can add static file or config files as needed
                .addAsResource("META-INF/persistence.xml")
                .addAsResource("META-INF/load.sql")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println(warToDeploy.toString(true));
        return warToDeploy;

    }

    /**
     * The test will actually be part of the war file - packaged inside the war file as managed bean.
     * We can therefore use inject as we are in CDI container.
     */
    @Inject
    BookRepository repo;

    @Inject
    AuthorRepository authorRepo;

    @Test
    public void test1_GetAllBook() {
        List<Book> books = repo.getAllBooks();
        //we expect 3 books, because we insert 3 via META-INF/load.sql
        Assert.assertEquals(3, books.size());
    }

    @Test
    public void test2_SaveNOK() {
        Book newBook = new Book();
        newBook.setTitle("Testing for dummies");
        try {
            //it is not possible to save book without author, ergo we expect an exception
            repo.persist(newBook);
            Assert.fail();
        } catch(Exception e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void test3_saveOk() {
        Book newBook = new Book();
        newBook.setTitle("Testing for dummies");
        //we can do this, because we have inserted author via test data META-INF/load.sql
        Author author = authorRepo.getById(10001);
        newBook.setAuthor(author);
        repo.persist(newBook);
        Assert.assertTrue(newBook.getId() != null);
        Assert.assertEquals("Testing for dummies", newBook.getTitle());
        Book bookLoadedFromDb = repo.getById(newBook.getId());
        //we should be able to find the new book by id now
        Assert.assertNotNull(bookLoadedFromDb);

    }
}
