-- this file is executued automatically on deployment - see persistence.xml
insert into author(id, name) values (10000, 'Sun Tzu');
insert into author(id, name) values (10001, 'Unknown Developer')
insert into book(id, title, description, author_id) values (9000, 'Joy of testing', 'It is a thin book, but very useful', 10001);
insert into book(id, title, description, author_id) values (9001, 'Art of war', 'The ultimate guide for warfare or office politics', 10000);
insert into book(id, title, description, author_id) values (9002, '10x programmer', 'Good programmers are 10x better then average onse, Myth or truth', 10001);
insert into book_tags(book_id, tag) values(9000, 'software');
insert into book_tags(book_id, tag) values(9000, 'testing');
insert into book_tags(book_id, tag) values(9000, 'dev');
insert into book_tags(book_id, tag) values(9001, 'chinese');
insert into book_tags(book_id, tag) values(9001, 'strategy');
insert into book_tags(book_id, tag) values(9001, 'war');
insert into book_tags(book_id, tag) values(9002, 'sofware');
insert into book_tags(book_id, tag) values(9002, 'dev');