package com.acme.trainings.domain.repositories;

import com.acme.trainings.domain.model.Book;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Respoitory aka DAO aka data layer.
 * This class acts as data access service, it is marked as Stateless = which means it is and EJB managed bean,
 * with transaction support(by default, you can control it using @TransactionAttribute annotation on methods).
 * Every EJB is CDI bean as well, so you can use standard @Inject to pass them to other beans.
 */
@Stateless
public class BookRepository {

    // Entity manager is our contact to the database
    @PersistenceContext
    EntityManager em;

    public List<Book> getAllBooks() {
        //the query bellow is written in JQPL(HQL) which is an object based query language similar to sql
        //what the query says:  I want all columns from all rows of table Book plus book's author, and for each result row, map it to Book class
        //that means: book.tags will not be loaded from db AND author books will not be loaded - book.getAuthor().getBooks().size = exception
        return this.em.createQuery("select b from Book b left join fetch b.author", Book.class)
                .getResultList();
    }

    //This transaction attribute is the default on Stateless bean, just for illustration
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(Book newBook) {
        //persist is for creating new items(returns void, directly modifies the object we pass it)
        this.em.persist(newBook);
    }

    public Book update(Book booktoUpdate) {
        //merge is update of existing items(returns a copy of the original argument, important if you need to do something with the entity after update)
        return this.em.merge(booktoUpdate);
    }

    public Book getById(long id) {
        //again JPQL, that basically reads: I want all columns from table Book, where id matches my parameters,
        //and for each result, map the row to Book class
        return em.createQuery("select b from Book b where b.id =:id", Book.class)
                //we defined a named parameter in the query '=:id', so we need to pass its value
                .setParameter("id", id)
                //getSingleResult with throw a NoResultException if no db row matches the criteria
                //or NonUniqueResultException if there is more than 1 row that matches
                .getSingleResult();
    }

    //If you explicitly do not want to have transaction for some method(e.g. for selects you do not need one)
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Book getByIdFull(long id) {
        //joins are similar to sql, but instead of specifying JOIN on a.pk = b.fk, we just use object dot notation,
        //as hibernate knows by which columns the tables are connected
        //if we want the joined collection in the result(in this case tags), then we need to add FETCH to the join clause
        return em.createQuery("select b from Book b left join fetch b.author a left join fetch b.tags where b.id =:id", Book.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public void deleteById(long id) {
        //we first find the book by id(using built-in find method, but we could also use our getById method)
        //end then we pass it to remove method
        em.remove(em.find(Book.class, id));
    }
}
