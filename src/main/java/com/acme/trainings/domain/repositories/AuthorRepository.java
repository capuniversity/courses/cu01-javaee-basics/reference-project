package com.acme.trainings.domain.repositories;

import com.acme.trainings.domain.model.Author;
import com.acme.trainings.domain.model.Book;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

//we want this to be managed bean and with transactions = EJB stateless
@Stateless
public class AuthorRepository {
    @PersistenceContext
    EntityManager em;

    public Author getById(long id) {
        return em.find(Author.class, id);
    }

    //This transaction attribute is the default on Stateless bean, just for illustration
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(Author newAuthor) {
        //persist is for creating new items(returns void, directly modifies the object we pass it)
        this.em.persist(newAuthor);
    }
}
