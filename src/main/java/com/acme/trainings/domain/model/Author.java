package com.acme.trainings.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
//You cae use this to customize the table name, add DB indexes if you need
//in most cases, you would defined indexes and constraints external - via liquibase for example
@Table(name="author", indexes = {@Index(columnList = "name")})
@Getter
@Setter
//every entity needs to be POJO = getters and setters, no argsconstructor
public class Author {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    //one to many just for illustration, in real life its many-to-many
    //we define this relationship as lazily loaded = we do not need the books everytime we load the author
    //and we add cascade remove, so that if we delete the author, we also delete all his book
    //mappedBy attribute specifies which field in Book class is mapping back to Author
    //as this is onetomany, the 'many' side(books) is the owner of teh relationship = it has the foreign key
    //this has implications on how we can insert the data(i.e. at the time of persist, it is teh Book instance
    // that must have to correct association info)
    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.REMOVE, mappedBy = "author")
    private Set<Book> books = new HashSet<>();
}
