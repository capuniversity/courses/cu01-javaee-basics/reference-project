package com.acme.trainings.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Book {
    @Id @GeneratedValue
    private Long id;
    private String title;
    private String description;
    //bidirectional one-2-mauy/many-2-one
    //Optional false, because we can not save a book without author
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    //Join column specifies the association link, controls the Foreign key in DB
    @JoinColumn(name = "author_id")
    private Author author;

    //Element collectipn is used, if you have an association to a collection of primitive values(or embedables)
    @ElementCollection(fetch = FetchType.LAZY)
    //we need some extra table if we want to store multiple tags for each book
    //in that table, we need an FK for book - we name it book_id
    @JoinTable(name = "book_tags",joinColumns = {@JoinColumn(name = "book_id")})
    //Collumn in this case specifies, what is the column name that will store the actual value of the strings in the set
    @Column(name="tag")
    private Set<String> tags = new HashSet<>();
}
