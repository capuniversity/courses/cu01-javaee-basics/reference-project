package com.acme.trainings.rs.model;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//simple POJO representing our author entity, but without books association.
public class AuthorWithoutBooks {

    private Long id;
    private String name;
}
