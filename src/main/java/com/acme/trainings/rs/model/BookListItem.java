package com.acme.trainings.rs.model;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO = data transfer object, aka HTTP model pojo representing book with the basic info(without all the associations).
 * Say we have a simple admin UI where we want ot see list of books, and then we can click on the book and open its detail.
 * This POHO would represent one line in the table.
 */
@Getter
@Setter
public class BookListItem {
    private Long id;
    private String title;
    private String description;
    private String authorName;
}
