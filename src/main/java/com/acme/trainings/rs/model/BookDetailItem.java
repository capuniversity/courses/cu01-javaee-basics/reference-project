package com.acme.trainings.rs.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * DTO for book with additional info - book tags, and author
 */
@Getter
@Setter
public class BookDetailItem {
    private Long id;
    private String title;
    private String description;
    private AuthorWithoutBooks author;
    private Set<String> bookTags;
}
