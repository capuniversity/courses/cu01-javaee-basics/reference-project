package com.acme.trainings.rs.controllers;

import com.acme.trainings.domain.model.Book;
import com.acme.trainings.domain.repositories.BookRepository;
import com.acme.trainings.rs.mappers.BookMapper;
import com.acme.trainings.rs.model.BookListItem;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class BooksRestController {

    // to get an instance of any managed bean from the container, use @Inject(also supported in Spring, although @Autowired is more commonly used)
    @Inject
    BookRepository repo;

    //we can inject the mapper, because it is managed bean(@Mapper(componentModel = "cdi"))
    @Inject
    BookMapper mapper;

    @GET
    @Path("")//this is implicit, we do not need to write it
    public List<BookListItem> getAllBooks() {
        List<Book> resultsFromDb = repo.getAllBooks();
        //we fetch results from db, and now we need to convert them to our HTTP layer model
        //in this case we use lambda on stream, but could be a regular loop as well
        //reads as : for each result from DB, map it to BookListItem using our mapper and return the list of results
        return resultsFromDb.stream().map(b-> mapper.map(b)).collect(Collectors.toList());
    }

    @GET
    @Path("/{id}")
    //We defined path parameter named id, so we need to annotate the method parem accordingly
    public Response getbyId(@PathParam("id") long id) {
        return Response.ok(mapper.mapToDetail(repo.getByIdFull(id))).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteById(@PathParam("id") long id) {
        repo.deleteById(id);
        return Response.noContent().build();
    }
}
