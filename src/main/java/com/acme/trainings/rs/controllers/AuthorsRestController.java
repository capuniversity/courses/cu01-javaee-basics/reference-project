package com.acme.trainings.rs.controllers;

import com.acme.trainings.domain.model.Author;
import com.acme.trainings.domain.repositories.AuthorRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST endpoint for Author API.
 * We annotate class with produces/consumes json so that all methods inherit it
 */
@Path("/authors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class AuthorsRestController {

    @Inject
    AuthorRepository repo;

    @POST
    //Path is default ""
    //consumes taken from class
    //we have one param, without any annotation - so it will be unmarshaled from request body
    public Response createAuthor(Author newAuthor) {
        repo.persist(newAuthor);
        //persist method modifies the object we pass it, so now the author has ID generated
        return Response.ok(newAuthor).build();
    }

    @GET
    @Path("{id}")//Path param, needs matching PathParam annotation in method argument
    public Response getAuthorById(@PathParam("id") long id) {
        //select a from Author a where a.id=:id
        Author authorFromDB = repo.getById(id);
        return Response.ok(authorFromDB).build();
    }
}
