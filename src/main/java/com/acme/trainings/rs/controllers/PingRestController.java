package com.acme.trainings.rs.controllers;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Path("/ping")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class PingRestController {

    @GET
    //@Path("") this is implicit if not specified
    //@Produces inherited from class if not provided
    //Final REST URL = "server:port/<web-context>/<JaxrsConfigPath>/<Controller-path>/<Method-path>"
    public String ping() {
        return "PONG";
    }

    @GET
    @Path("plainText")
    @Produces(MediaType.TEXT_PLAIN)
    //Final REST URL = "server:port/<web-context>/<JaxrsConfigPath>/<Controller-path>/<Method-path>"
    public Response pingAsPlainText() {
        return Response.ok("PONG").build();
        //Response.ok(entity).build() is equivalent of just returning the entity
    }

    @GET
    @Path("pingComplex")
    //@Produces inherited from class if not provided
    public Response pingAsComplexObject() {
        Map<String, Object> data = new HashMap<>();
        data.put("state", "ok");
        data.put("healthy", true);
        //we return java hashmap as body, JAXRS will find a marshaller(Jackson) and ask it to convert the java object to JSON
        return Response.ok(data).build();
        //again, we could also just return data directly(and change the return type of course)
    }

}
