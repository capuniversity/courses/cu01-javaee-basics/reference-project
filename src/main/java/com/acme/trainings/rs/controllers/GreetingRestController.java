package com.acme.trainings.rs.controllers;

import jdk.nashorn.internal.objects.annotations.Getter;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/hello")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class GreetingRestController {

    /**
     * The following method will respond with HTTP 200 OK if name is provided, and with 400 Bad Request if not.
     * @param name your name
     * @return greeting
     */
    @GET
    //Method Path is implicitly set to ""
    //Consumes/Provides is taken from class level
    //Final REST URL = "server:port/<web-context>/<JaxrsConfigPath>/<Controller-path>/<Method-path>"
    //Query params are not used for URL matching, so they are optional, if you do no provide them, their value will be NULL for objects or whatever the default value for given primitive type is
    public Response greet(@QueryParam("name") String name, @QueryParam("someBoolean")boolean someBoolean, @QueryParam("someNumber") Long someNumber) {
        //if we call the method with URL: /demo/hello?name=something
        //then name = "somename", someBoolean = false(default for bool), someNumber = null(it is a Big Long/ Object, ergo null instead of 0)
        if (name == null || name.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity("You did not tell me your name").build();
        }
        return Response.ok("Nice to meet you " + name).build();
    }
}
