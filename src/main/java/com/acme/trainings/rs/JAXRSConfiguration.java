package com.acme.trainings.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Entrypoint for JAXRS.
 * By default, it will work with auto-discovery of all classes annotated with <code>@Path()</code> and make them available under the <code>@ApplicatonPath</code>
 */
@ApplicationPath("/")
public class JAXRSConfiguration extends Application {

}
