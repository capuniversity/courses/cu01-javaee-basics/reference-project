# Demo JEE7 project

## Build & run

Build  
The project is built by maven, resulting in war file . To build it, simply run: `mvn clean package`. 
You will need an app server to run the war file. 

Deploy manually  
Download any JEE & server and deploy the war file. On wildfly/jboss that means copy the war file from `target` to `SERVER_DIR/standalone/deployments/`.

Deploy via maven:  
For Wildfly/JBoss you can also use maven plugin to trigger the deployment: 
```
mvn wildfly:deploy
``` 
Note: the application server needs to be running before you trigger this.

Deploy via intelliJ  
Create a new run config, based on JBoss local and add your project as deployment.

Once deployed, you should be able to access the app via: 
http://localhost:8080/demo-backend/ping
http://localhost:8080/demo-backend/books


## Import into IntelliJ
File > New project from existing sources > Import from external model > Maven

## Running tests

## Configuration

### Web context

Web context will be implicitly set to the name of the .war file as deployed to JBoss. 
To change the web context, define a file `src/main/webapp/WEB-INF/jboss-web.xml` with the following contents

```$xml
<?xml version="1.0" encoding="UTF-8"?>
<jboss-web>
    <context-root>my-web-context-rs</context-root>
</jboss-web>
```